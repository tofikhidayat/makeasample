
jQuery(document).ready(function($) {


    var getDestinationURL = function getDestinationURL(urlParam) {
        var DestinationURL = decodeURIComponent(window.location.search.substring(1)),
            URLvar = DestinationURL.split('&'),
            destinationName, i;
        for (i = 0; i < URLvar.length; i++) {
            destinationName = URLvar[i].split('=');
            if (destinationName[0] === urlParam) {
                return destinationName[1] === undefined ? true : destinationName[1];
            }
        }
    };
    var lokasi_url = getDestinationURL('animation');
    if (lokasi_url == "true") {
        $("a[href]").click(function(event) {
            var change = $(this).attr('href');
            if (change != "#") {
                $(this).removeAttr('href');

                $(this).attr('href', change + "?animation=true");

            } else {
                $("*[data-aos]").removeAttr('data-aos');
            }
        });

    }
    else{
        $("*[data-aos]").removeAttr('data-aos');
    }

});


$(window).scroll(function(event) {
var scroll = $(window).height();
if ($(window).scrollTop() > scroll - 100) {
    $(".to-top").fadeIn("fast");
}
else
{
$(".to-top").fadeOut("fast");   
}
});

$(document).on('click', '.go-top', function(event) {
     $("html, body").animate({scrollTop: 0}, 700)

});

AOS.init({
 duration: 1200
});
