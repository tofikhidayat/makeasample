var bordercolor = "#00D185";
  var ctx = document.querySelector('.eth-chart').getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
    
gradient.addColorStop(0, '#B4FADE');
    gradient.addColorStop(0.25, '#fff');
gradient.addColorStop(0.5, '#fff');

  var config = {
    type: 'line',
    data: {
      labels: ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun','null'],
      datasets: [{
        
        label: '  ',
        backgroundColor: gradient,
        borderColor: bordercolor,
        borderWidth:"5",
        fill: true,
        fillColor:"#fff",
        pointColor:"#fff",
        pointBackgroundColor:"#fff",
        pointBorderWidth: 4,
         pointRadius: 6,
          pointHoverRadius: 9,
           pointHoverBorderWidth: 2,
        data:  ethdata ,

      }]
    },
    options: {
      legend: {
        display: false
    },
    tooltips: {
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'transparent',
                        backgroundColor: 'transparent',
                        display: 'false'
                    }
                },
                labelTextColor:function(tooltipItem, chart){
                    return 'red';
                }
            }},
     datasetStrokeWidth : 0,
            pointDotStrokeWidth : 0,
            tooltipFillColor: "#fff",
            tooltipFontStyle: "normal",
      responsive: true,
      title: {
        display:false,
        text: 'helo'
      },
      scales: {
        xAxes: [{
          display: false,
        }],
        yAxes: [{
          display: false, 
          type: 'logarithmic',
        }]

      }
    }
  };


    window.myLine = new Chart(ctx, config);


//btc-chart


var bordercolor = "#00D185";
  var ctx = document.querySelector('.btc-chart').getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
    
gradient.addColorStop(0, '#B4FADE');
    gradient.addColorStop(0.25, '#fff');
gradient.addColorStop(0.5, '#fff');

  var config = {
    type: 'line',
    data: {
      labels: ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun','null'],
      datasets: [{
        
        label: '  ',
        backgroundColor: gradient,
        borderColor: bordercolor,
        borderWidth:"5",
        fill: true,
        fillColor:"#fff",
        pointColor:"#fff",
        pointBackgroundColor:"#fff",
        pointBorderWidth: 4,
         pointRadius: 6,
          pointHoverRadius: 9,
           pointHoverBorderWidth: 2,
        data:  btcdata ,

      }]
    },
    options: {
      legend: {
        display: false
    },
    tooltips: {
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'transparent',
                        backgroundColor: 'transparent',
                        display: 'false'
                    }
                },
                labelTextColor:function(tooltipItem, chart){
                    return 'red';
                }
            }},
     datasetStrokeWidth : 0,
            pointDotStrokeWidth : 0,
            tooltipFillColor: "#fff",
            tooltipFontStyle: "normal",
      responsive: true,
      title: {
        display:false,
        text: 'helo'
      },
      scales: {
        xAxes: [{
          display: false,
        }],
        yAxes: [{
          display: false, 
          type: 'logarithmic',
        }]

      }
    }
  };


    window.myLine = new Chart(ctx, config);




//ripple-chart


var bordercolor = "#E81E8D";
  var ctx = document.querySelector('.ripple-chart').getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
    
gradient.addColorStop(0, '#FFF0F8');
    gradient.addColorStop(0.25, '#fff');
gradient.addColorStop(0.5, '#fff');

  var config = {
    type: 'line',
    data: {
      labels: ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun','null'],
      datasets: [{
        
        label: '  ',
        backgroundColor: gradient,
        borderColor: bordercolor,
        borderWidth:"5",
        fill: true,
        fillColor:"#fff",
        pointColor:"#fff",
        pointBackgroundColor:"#fff",
        pointBorderWidth: 4,
         pointRadius: 6,
          pointHoverRadius: 9,
           pointHoverBorderWidth: 2,
        data:  rippledata ,

      }]
    },
    options: {
      legend: {
        display: false
    },
    tooltips: {
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'transparent',
                        backgroundColor: 'transparent',
                        display: 'false'
                    }
                },
                labelTextColor:function(tooltipItem, chart){
                    return 'red';
                }
            }},
     datasetStrokeWidth : 0,
            pointDotStrokeWidth : 0,
            tooltipFillColor: "#fff",
            tooltipFontStyle: "normal",
      responsive: true,
      title: {
        display:false,
        text: 'helo'
      },
      scales: {
        xAxes: [{
          display: false,
        }],
        yAxes: [{
          display: false, 
          type: 'logarithmic',
        }]

      }
    }
  };


    window.myLine = new Chart(ctx, config);
///date selector



var date = new Date();
var days = date.getDay();
$(".days li:nth-child("+days+")").css('color', '#41475B').css('font-weight', '500');