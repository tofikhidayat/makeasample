var bordercolor = "#00D185";
  var ctx = document.querySelector('.eth-chart-1').getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
   var ctxa = document.querySelector('.eth-chart-2').getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
    var ctxb = document.querySelector('.eth-chart-3').getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
     var ctxc = document.querySelector('.eth-chart-4').getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
      var ctxd = document.querySelector('.eth-chart-5').getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);

    
gradient.addColorStop(0, '#B4FADE');
    gradient.addColorStop(0.25, '#fff');
gradient.addColorStop(0.5, '#fff');

  var config = {
    type: 'line',
    data: {
      labels: ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun','null'],
      datasets: [{
        
        label: '  ',
        backgroundColor: gradient,
        borderColor: bordercolor,
        borderWidth:"5",
        fill: true,
        fillColor:"#fff",
        pointColor:"#fff",
        pointBackgroundColor:"#fff",
        pointBorderWidth: 4,
         pointRadius: 6,
          pointHoverRadius: 9,
           pointHoverBorderWidth: 2,
        data:  ethdata ,

      }]
    },
    options: {
      legend: {
        display: false
    },
    tooltips: {
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'transparent',
                        backgroundColor: 'transparent',
                        display: 'false'
                    }
                },
                labelTextColor:function(tooltipItem, chart){
                    return 'red';
                }
            }},
     datasetStrokeWidth : 0,
            pointDotStrokeWidth : 0,
            tooltipFillColor: "#fff",
            tooltipFontStyle: "normal",
      responsive: true,
      title: {
        display:false,
        text: 'helo'
      },
      scales: {
        xAxes: [{
          display: false,
        }],
        yAxes: [{
          display: false, 
          type: 'logarithmic',
        }]

      }
    }
  };


    window.myLine = new Chart(ctx, config);
    window.myLine = new Chart(ctxa, config);
    window.myLine = new Chart(ctxb, config);
    window.myLine = new Chart(ctxc, config);
    window.myLine = new Chart(ctxd, config);


//btc-chart
window.onload = function(){

var bordercolor = "#00D185";
  var ctx =document.querySelector(".btc-chart-1").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
  var ctxa =document.querySelector(".btc-chart-2").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
   var ctxb =document.querySelector(".btc-chart-3").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
    var ctxc =document.querySelector(".btc-chart-4").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
     var ctxd =document.querySelector(".btc-chart-5").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
 
gradient.addColorStop(0, '#B4FADE');
    gradient.addColorStop(0.25, '#fff');
gradient.addColorStop(0.5, '#fff');

  var config = {
    type: 'line',
    data: {
      labels: ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun','null'],
      datasets: [{
        
        label: '  ',
        backgroundColor: gradient,
        borderColor: bordercolor,
        borderWidth:"5",
        fill: true,
        fillColor:"#fff",
        pointColor:"#fff",
        pointBackgroundColor:"#fff",
        pointBorderWidth: 4,
         pointRadius: 6,
          pointHoverRadius: 9,
           pointHoverBorderWidth: 2,
        data:  btcdata ,

      }]
    },
    options: {
      legend: {
        display: false
    },
    tooltips: {
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'transparent',
                        backgroundColor: 'transparent',
                        display: 'false'
                    }
                },
                labelTextColor:function(tooltipItem, chart){
                    return 'red';
                }
            }},
     datasetStrokeWidth : 0,
            pointDotStrokeWidth : 0,
            tooltipFillColor: "#fff",
            tooltipFontStyle: "normal",
      responsive: true,
      title: {
        display:false,
        text: 'helo'
      },
      scales: {
        xAxes: [{
          display: false,
        }],
        yAxes: [{
          display: false, 
          type: 'logarithmic',
        }]

      }
    }
  };


   window.myLine = new Chart(ctx, config);
    window.myLine = new Chart(ctxa, config);
    window.myLine = new Chart(ctxb, config);
    window.myLine = new Chart(ctxc, config);
    window.myLine = new Chart(ctxd, config);

}


//ripple-chart


var bordercolor = "#E81E8D";

 var ctx =document.querySelector(".ripple-chart-1").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
  var ctxa =document.querySelector(".ripple-chart-2").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
   var ctxb =document.querySelector(".ripple-chart-3").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
    var ctxc =document.querySelector(".ripple-chart-4").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
     var ctxd =document.querySelector(".ripple-chart-5").getContext('2d'),gradient = ctx.createLinearGradient(0, 0, 0, 500);
    
gradient.addColorStop(0, '#FFF0F8');
    gradient.addColorStop(0.25, '#fff');
gradient.addColorStop(0.5, '#fff');

  var config = {
    type: 'line',
    data: {
      labels: ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun','null'],
      datasets: [{
        
        label: '  ',
        backgroundColor: gradient,
        borderColor: bordercolor,
        borderWidth:"5",
        fill: true,
        fillColor:"#fff",
        pointColor:"#fff",
        pointBackgroundColor:"#fff",
        pointBorderWidth: 4,
         pointRadius: 6,
          pointHoverRadius: 9,
           pointHoverBorderWidth: 2,
        data:  rippledata ,

      }]
    },
    options: {
      legend: {
        display: false
    },
    tooltips: {
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'transparent',
                        backgroundColor: 'transparent',
                        display: 'false',
                        color:"cyan",
                    }
                },
                labelTextColor:function(tooltipItem, chart){
                    return 'red';
                },
                 labelFontStyle:function(tooltipItem, chart){
                    return 'normal';
                }
            }},
     datasetStrokeWidth : 0,
            pointDotStrokeWidth : 0,
            tooltipFillColor: "#fff",
            tooltipFontStyle: "normal",
      responsive: true,
      title: {
        display:false,
        text: 'helo'
      },
      scales: {
        xAxes: [{
          display: false,
        }],
        yAxes: [{
          display: false, 
          type: 'logarithmic',
        }]

      }
    }
  };

window.myLine = new Chart(ctx, config);
    window.myLine = new Chart(ctxa, config);
    window.myLine = new Chart(ctxb, config);
    window.myLine = new Chart(ctxc, config);
    window.myLine = new Chart(ctxd, config);
///date selector



var date = new Date();
var days = date.getDay();
$(".days li:nth-child("+days+")").css('color', '#41475B').css('font-weight', '500');
$(".days li").click(function(event) {
  
  $(this).parent().find('li').css('color', '#CFD6EA').css('font-weight', 'normal');
  $(this).css('color', '#41475B').css('font-weight', '500');
});